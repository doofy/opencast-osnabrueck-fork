# Notice

This is a fork of the [Official Opencast Matterhorn
Repository](https://bitbucket.org/opencast-community/matterhorn).


This fork contains additional features and fixes developed at the [University
of Osnabrück](http://www.virtuos.uos.de) which are not yet merged into the
official Opencast Matterhorn repository.
